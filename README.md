Current World Releases

Note: current_release.json contains the World Archive information:

```json
{
  "ArchivePath": "releases/World-db-v0.3.1_DF_5.1.zip",
  "SQLFile": "World-db-v0.3.1_DF_5.1.sql",
  "Hash": "4CE4884B63178758788C2FE2876E56885DFAFCD2EB624B3DE3F346CD730B6FD9"
}
```